// api/types.js

/**
 * 网站使用的 API 列表
 */
const api: {
  [k: string]: string;
} = {
  GET_SITE_INFO: "siteinfo",
};

export default api;
