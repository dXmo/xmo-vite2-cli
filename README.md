# Xmo-Vite2-Cli

本项目为 `Vite2` 的脚手架，项目添加了工程化质量管理的内容，以及 `vue-router` 和 `vuex` 这两项经常会被使用的插件。

> 该脚手架劫持了 `git commit` 事件，帮助你输出规整的 `git commit` 。请使用 `yarn commit` 代替 `git add . && git commit -m *`。
> 建议你使用 `vscode` 作为 `IDE` 进行编码，并且在任意 `.vue` `.ts` `.js` `.html` `.css` `.scss` 文件中右键设置格式化方案，实现保存即格式化

## 依赖或插件

使用 `yarn` 或 `npm` 安装插件，推荐 `yarn` 。

1. vite2
2. typescript
3. vue-router && vuex
4. axios + mockjs
5. eslint + recommend
6. husky@4

## 使用

### 运行与部署

```bash
# 热启动
yarn dev

# 部署
yarn build
```

### git

```bash
# commit
yarn commit

# push
git push
```

## 目录结构

### src

```bash
src
├─@types
├─api
│  └─mock
├─assets
├─components
├─controller
├─layout
├─router
├─store
│  └─user
└─views
```

#### @types

全局类型定义，可以用来定义全局模型。

#### api

`axios` 相关，用于与后台执行交互。

#### assets

存放静态文件或 `css`、`scss` 文件。

#### components

存储全局共享组件。

#### layout

存放布局组件。

#### router

`vue-router` 配置。

#### store

`vuex` 配置。

#### views

存放页面

### 根目录

```bash
xmo-vite2-cli
├─.eslintignore
├─.eslintrc.js
├─.gitignore
├─.huskyrc.json
├─commitlint.config.js
├─index.html
├─package.json
├─README.md
├─.prettierignore
├─.prettierrc.json
├─tsconfig.json
├─vite.config.ts
├─yarn.lock
```

#### eslint

`.eslintignore` 忽略
`.eslintrc.js` 配置

#### prettier

`.prettierignore` 忽略
`.prettierrc.json` 配置

#### husky

`huskyrc` 配置

#### commitlint

`commitlint.config.js` commitlint配置
