declare module "model" {
  export interface SingleExample {
    name: string;
    id: number;
  }

  export interface SiteInfo {
    name: string;
    slogan: string;
  }
}