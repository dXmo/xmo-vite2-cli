import request from "./";
import api from "./types";
import { AxiosResponse } from "axios";
import { SiteInfo } from "model";

export const siteInfoReq: Promise<AxiosResponse<SiteInfo>> = request.get(api.GET_SITE_INFO);
