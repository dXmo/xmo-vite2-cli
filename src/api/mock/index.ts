import Mock, { Random } from "mockjs";
import API from "@/api/types";
import { SiteInfo } from "model";

const baseUrl = 'http://127.0.0.1:7001/';

/**
 * 设置延时
 */
Mock.setup({
  timeout: "200-600", // 表示响应时间介于 200 和 600 毫秒之间，默认值是'10-100'。
});

const userInfo = () => {
  const item: SiteInfo = {
    name: "XmoPage",
    slogan: Random.cparagraph()
  };
  return item;
};

/**
 * Mock模拟数据
 */
export default () => {
  Mock.mock(baseUrl + API.GET_SITE_INFO, userInfo);
};

// 输出结果
