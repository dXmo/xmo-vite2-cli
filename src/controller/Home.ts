import { siteInfoReq } from "@/api/siteReq";

export const initSiteInfo = async () => {
  return await siteInfoReq.then((res) => {
    return res.data;
  }).catch((__) => {
    alert(__);
    return null;
  });
};